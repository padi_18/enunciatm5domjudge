import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolucioEnunciatTest {

	@Test
	void testDemanaOpcio1() {
		String res = SolucioEnunciat.demanaOpcio1();
		assertEquals("paper", res);
		assertEquals("pedra", res);
		assertEquals("tisores", res);
	}

	@Test
	void testDemanaOpcio2() {
		String res = SolucioEnunciat.demanaOpcio2();
		assertEquals("paper", res);
		assertEquals("pedra", res);
		assertEquals("tisores", res);
	}
	
}
