import java.util.Scanner;

/**
 * El meu enunciat tractarà sobre el pedra paper tisores, un joc clàssic que pot ser entretingut.
 * Consisteix en que dos jugadors s'enfrentin i amb una sèrie de regles guanyar la partida. Però
 * aquí s'ha de tenir sort...
 *
 * @author Xavi Padilla
 * @since 18/02/2020
 * @version 1.0
 */
public class SolucioEnunciat {
	
	/**
	 * Declaració del Scanner.
	 */
	
    public static Scanner sc = new Scanner(System.in);

    /**
     * Funció principal on executarem el joc i tancarem el Scanner.
     * 
     * @param args Paràmetre bàsic.
     */
    
    public static void main(String[] args) {
        
        joc();
        
        sc.close();

    }

    /** 
     * Mètode on executarem el joc principal.
    */

    public static void joc() {

        int casosDeProva = sc.nextInt();
        
        int i = 0;

        while (i < casosDeProva) {

            condicions();
            
            i++;
            
        }
        
    }

    /**
     * Mètode per demanar l'opció que vol l'usuari 1.
     * 
     * @return L'opció escollida pel jugador 1.
     */

    public static String demanaOpcio1() {

        String opcio = sc.next();

        return opcio;
        
    }

    /**
     * Mètode per escollir l'opció que vol l'usuari 2.
     * 
     * @return L'opció escollida per el jugador 2.
     */

    public static String demanaOpcio2() {

        String opcio2 = sc.next();

        return opcio2;
        
    }

    /**
     * Mètode per fer les restriccions del joc. Aqui veiem quina és l'opció que 
     * guanya, perd, o empata.
     */

    public static void condicions() {

        String jugador1 = demanaOpcio1().toLowerCase();
        String jugador2 = demanaOpcio2().toLowerCase();

        if (jugador1.equals("pedra") && jugador2.equals("tisores")) {

            guanyat();
            
        } else if (jugador1.equals("pedra") && jugador2.equals("paper")){

            perdut();
            
        } else if (jugador1.equals("pedra") && jugador2.equals("pedra")) {

            empat();

        } else if (jugador1.equals("paper") && jugador2.equals("tisores")) {

            perdut();

        } else if (jugador1.equals("paper") && jugador2.equals("pedra")) {

            guanyat();

        } else if (jugador1.equals("paper") && jugador2.equals("paper")) {

            empat();

        } else if (jugador1.equals("tisores") && jugador2.equals("pedra")) {

            perdut();

        } else if (jugador1.equals("tisores") && jugador2.equals("paper")) {

            guanyat();

        } else if (jugador1.equals("tisores") && jugador2.equals("tisores")) {

            empat();

        }
        
    }

    /**
     * Mètode que retorna un missatje de guanyar.
     */

    public static void guanyat() {
        
        System.out.println("HAS GUANYAT");

    }

    /**
     * Mètode que retorna un missatje de pèrdua.
     */

    public static void perdut() {

        System.out.println("HAS PERDUT");
        
    }

    /**
     * Mètode que mostra un missatje d'empat.
     */

    public static void empat() {

        System.out.println("EMPAT");
        
    }

}